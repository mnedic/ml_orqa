import numpy as np
import cv2
import screeninfo
import time
import sys

screen_id = 1

# get the size of the screen
test = screeninfo.get_monitors()
# print(test)
# exit()
screen = test[screen_id]
N = len(test)

if(N == 1):
    print("OE not detected as second display!")
    cv2.waitKey()
    exit()
elif(N ==2):
    print("OE detected! Initializing image projection to OE.")
else:
    print("Unexpected number of displays!")
    cv2.waitKey()
    exit()

img_path = sys.argv[1]
img = cv2.imread(img_path)

window_name = 'ProjectScreen'

cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
cv2.moveWindow(window_name, screen.x - 1, screen.y - 1)
cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

cv2.imshow(window_name, img)
cv2.waitKey()
cv2.destroyAllWindows()
