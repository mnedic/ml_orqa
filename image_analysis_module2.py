# Image Analysis Module (IAM) contains:
# 1. mkdir_analysis()
# 2. import_display_images()
# 3. apply_mask()
# 4. otsu_cut_and_mask()
# 5. magic_flip_test() --> MASTER_flip-test

import imageio

import numpy as np
from matplotlib import pyplot as plt
from cv2 import boundingRect
from scipy import ndimage
from scipy.signal import find_peaks
from skimage.filters import threshold_otsu
import os
from os.path import join

from skimage.color import rgb2gray
from skimage import img_as_ubyte
from skimage.util import invert
from skimage.measure import label, regionprops

from datetime import datetime
import time
start_time = time.time()

# 1
def mkdir_analysis(new_path):
    os.makedirs(new_path, exist_ok=True)

# 2
def import_display_images(mypath):
    onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(join(mypath, f))]

    paths = []
    for filepath in onlyfiles:
        paths.append(mypath + '\\' + filepath)

    return paths

# 3
def apply_mask(path, mask, mask_crop):
    try:
        image = imageio.imread(path)
    except ValueError:
        print("Unable to read the image! File missing or truncated.")
        return None, None, None, 1


    # Cropanje slike koja će ići na analizu, da se ubrza provedba koda
    # find where the signature is and make a cropped region
    points = np.argwhere(mask==0) # find where the black pixels are
    points = np.fliplr(points) # store them in x,y coordinates instead of row,col indices
    x, y, w, h = boundingRect(points) # create a rectangle around those points
    x, y, w, h = x-30, y-30, w+70, h+70 # make the box a little bigger
    image_crop = image.copy()
    image_crop = image_crop[y:y+h, x:x+w] # create a cropped region of the gray image

    # Primjena maske
    masked_image = image_crop.copy()
    masked_image[mask_crop>0] = 0
    # Masked image in grayscale
    masked_image_gray = rgb2gray(masked_image)

    return image_crop, masked_image, masked_image_gray, 0


# 4
def otsu_cut_and_mask(path):
    # Try-Except is applied for white image here (as first acquired image it is most susceptible to truncation due to limitations of cameras)
    try:
        im = imageio.imread(path)
    except ValueError:
        print("Unable to read the image! File missing or truncated.")
        return None, None, 1

    # Pretvorba slike (1) u greyscale
    im_g = rgb2gray(im)
    img = img_as_ubyte(im_g)

    # Uvođenje blura da se šum nakon otsu tehnike ne uvede u masku
    img = ndimage.gaussian_filter(img, 1)

    # Postavke za Otsu Threshholding
    threshold_global_otsu = threshold_otsu(img)
    global_otsu = img >= threshold_global_otsu

    # Masking:
    mask = global_otsu  # < 0.2
    mask = invert(mask)  # Maska se mora invertirati prije primjene na slici jer se slika mijenja samo gdje je maska True - bijela


    # Cropanje maske: find where the signature is and make a cropped region
    points = np.argwhere(mask == 0)  # find where the black pixels are
    points = np.fliplr(points)  # store them in x,y coordinates instead of row,col indices
    x, y, w, h = boundingRect(points)  # create a rectangle around those points
    x, y, w, h = x-30, y-30, w+70, h+70  # make the box a little bigger
    mask_crop = mask.copy()
    mask_crop = mask_crop[y:y+h, x:x+w]  # create a cropped region of the gray image

    #if (mask_crop.shape[0]>1000 and mask_crop.shape[1]>2000):     #za slike veće rezulucije 
    if (mask_crop.shape[0] > 500 and mask_crop.shape[1] > 500):   # za slike manje rezulucije
        return mask, mask_crop, 0
    else:
        labeled_mask = label(mask, background=5)
        clusters = regionprops(labeled_mask, img)

        # Sort clusters after descending area size - first is background, second is display, rest is noise
        area_list = []
        for cluster in clusters:
            area_list.append(cluster.area)

        area_tuple = [(cluster.area, cluster.label) for cluster in clusters]
        sorted_area_tuple = sorted(area_tuple, reverse=True)
        background = clusters[sorted_area_tuple[0][1]-1]
        display = clusters[sorted_area_tuple[1][1]-1]

        mask = np.ones((im.shape[0], im.shape[1]))
        for i, j in display.coords:
            mask[i, j] = 0

        # # Cropanje maske: find where the signature is and make a cropped region
        points = display.coords
        points = np.fliplr(points)  # store them in x,y coordinates instead of row,col indices
        x, y, w, h = boundingRect(points)  # create a rectangle around those points
        x, y, w, h = x-30, y-30, w+70, h+70  # make the box a little bigger
        mask_crop = mask.copy()
        mask_crop = mask_crop[y:y+h, x:x+w]  # create a cropped region of the gray image

        io.imshow(mask)
        io.show()
        return mask, mask_crop, 0




# 5
def magic_flip_test(masked_image_gray, mask, filename, path, report_path):
    image = masked_image_gray
    image = img_as_ubyte(image)
    
    mean = int((image.shape[0])/2)
  
    gnorm_mask = image
    slice = gnorm_mask[mean, :]
    #peaks,_ = find_peaks(invert(slice), distance=5)
    #peaks_down = [peak for peak in peaks if slice[peak] < 25]
    #peaks2, _ = find_peaks(slice, distance=5)
    x = range(0, slice.shape[0])
    plt.plot(slice)
    #plt.plot(peaks_down, slice[peaks_down], "x", color='red')
    plt.savefig('image_slice_lpmm_{}'.format(filename), bbox_inches='tight')
   





