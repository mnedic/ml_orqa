@echo off

   rem ******************  MAIN CODE SECTION
   set STARTTIME=%TIME%
   rem Your code goes here (remove the ping line)
   REM ping -n 4 -w 1 127.0.0.1 >NUL
   @echo off

   start "MAIN" /WIN /C cmd
   setlocal disableDelayedExpansion

   set myvar=%1
   echo %myvar%

   START "FOV" /b /WAIT python MASTER_FOV.py %myvar%
   echo FOV - Done (%DATE% %TIME%) >> timer.txt

   START "SHARPNESS" /b /WAIT python MASTER_sharpness.py %myvar%
   echo Sharpness - Done (%DATE% %TIME%) >> timer.txt

   REM START "SPECKS" /b /WAIT python MASTER_specks.py %myvar%
   rem cho Specks - Done (%DATE% %TIME%) >> timer.txt

   rem START "INTENSITY" /b /WAIT python MASTER_intensity.py %myvar%
   rem echo Intensity - Done (%DATE% %TIME%) >> timer.txt

   START "MOVE" /b /WAIT python moveAfterAnalysis.py %myvar%
   echo %myvar% moved to ANALYZED (%DATE% %TIME%) >> timer.txt

   set ENDTIME=%TIME%

   rem ******************  END MAIN CODE SECTION


   rem Change formatting for the start and end times
   for /F "tokens=1-4 delims=:.," %%a in ("%STARTTIME%") do (
      set /A "start=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
   )

   for /F "tokens=1-4 delims=:.," %%a in ("%ENDTIME%") do (
      set /A "end=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
   )

   REM rem Calculate the elapsed time by subtracting values
   REM set /A elapsed=end-start
   REM
   REM rem Format the results for output
   REM set /A hh=elapsed/(60*60*100), rest=elapsed%%(60*60*100), mm=rest/(60*100), rest%%=60*100, ss=rest/100, cc=rest%%100
   REM if %hh% lss 10 set hh=0%hh%
   REM if %mm% lss 10 set mm=0%mm%
   REM if %ss% lss 10 set ss=0%ss%
   REM if %cc% lss 10 set cc=0%cc%
   REM
   REM set DURATION=%hh%:%mm%:%ss%,%cc%
   REM
   REM echo ------------------------ >> timer.txt
   REM echo Analysis Done (%DATE% %TIME%) >> timer.txt
   REM echo Start    : %STARTTIME% >> timer.txt
   REM echo Finish   : %ENDTIME% >> timer.txt
   REM echo          --------------- >> timer.txt
   REM echo Duration : %DURATION% >> timer.txt
   REM echo. >> timer.txt
   REM
   REM EXIT
   REM OVDJE STAVI DA POZIVA POST
   REM START "" /b /WAIT python post.py

   REM START "" /b /WAIT php C:\\xampp\\htdocs\\OrqaTestProcess\\artisan schedule:run

   REM EXIT

REM PAUSE
