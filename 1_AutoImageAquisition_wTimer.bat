
@echo off

   set STARTTIME=%TIME%
   @echo off

   REM start "MAIN" /WIN cmd
   setlocal disableDelayedExpansion

   set myvar=%1
   REM cd /d \Acquisition-server
   echo "%myvar%"
   
   START "PREPARATION"  /b /wait python preparation.py %myvar%
   rem echo "Python prepration"
   REM goto:eof
   REM PAUSE

   set mypath=%cd%
   set fullpath=%mypath%\ACQUISITION\%myvar%
   echo %fullpath%
   REM goto:eof

   for /r %%F in (*pure*.png) do (
     echo "%%F"
     cd %fullpath%
     REM goto:eof
     START "CINEMA" /b python MASTER_projectScreen.py "%%F"
     REM START "CINEMA" /b Project_opencv.exe "%%F"

     echo "Executing image acquisition."
	 echo %fullpath% 
     START "L" /b R_BasicFunction_low.exe
     REM START "R" /b R_BasicFunction.exe
     timeout /t 5 /nobreak
     taskkill /F /FI "IMAGENAME eq R_BasicFunction_low.exe"
     REM taskkill /F /FI "IMAGENAME eq R_BasicFunction.exe"

     REM if /I %myvar:~6,-3%==L (
     REM   echo "Executing image acquisition for LEFT OE."
     REM   START "L" /b L_BasicFunction.exe
     REM   timeout /t 6 /nobreak
     REM   taskkill /F /FI "IMAGENAME eq L_BasicFunction.exe"
     REM
     REM   ) else (
     REM     echo "Executing image acquisition for RIGHT OE."
     REM     START "R" R_BasicFunction.exe
     REM     timeout /t 6 /nobreak
     REM     taskkill /F /FI "IMAGENAME eq R_BasicFunction.exe"
     REM     )

     taskkill /F /FI "WINDOWTITLE eq ProjectScreen"

  )
cd ..

set ENDTIME=%TIME%

rem ******************  END MAIN CODE SECTION


rem Change formatting for the start and end times
for /F "tokens=1-4 delims=:.," %%a in ("%STARTTIME%") do (
  set /A "start=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
)

for /F "tokens=1-4 delims=:.," %%a in ("%ENDTIME%") do (
  set /A "end=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
)

rem Calculate the elapsed time by subtracting values
set /A elapsed=end-start

rem Format the results for output
set /A hh=elapsed/(60*60*100), rest=elapsed%%(60*60*100), mm=rest/(60*100), rest%%=60*100, ss=rest/100, cc=rest%%100
if %hh% lss 10 set hh=0%hh%
if %mm% lss 10 set mm=0%mm%
if %ss% lss 10 set ss=0%ss%
if %cc% lss 10 set cc=0%cc%

set DURATION=%hh%:%mm%:%ss%,%cc%

echo Acquisition of %myvar% Done (%DATE% %TIME%) >> timer.txt
echo Start    : %STARTTIME% >> timer.txt
echo Finish   : %ENDTIME% >> timer.txt
echo          --------------- >> timer.txt
echo Duration : %DURATION% >> timer.txt
echo. >> timer.txt

REM PAUSE
REM echo %cd%
cd..
START "" /b /wait python temp_clean.py  
START "" /b /wait python rename.py %fullpath%
REM PAUSE
REM EXIT
