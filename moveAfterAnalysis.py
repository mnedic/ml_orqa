import sys
import os
from shutil import move

OE_name = sys.argv[1]
dir_path = os.path.dirname(os.path.realpath(__file__))

src = dir_path + "\\ACQUISITION"
dest = dir_path + "\\ANALYZED"
# print(OE_name, "\n", src, "\n", dest)

print("\n****************************** MOVE ******************************")


if os.path.isdir(src + "\\" + OE_name) is False:
    print("Cannot move {}. Source directory not found in ACQUISITION!".format(OE_name))
    quit()

if os.path.isdir(dest) is False:
    os.makedirs(dest)

if os.path.isdir(dest + "\\" + OE_name) is True:
    print("{} already exist in destination directioy!".format(OE_name))
    quit()

move(src + "\\" + OE_name, dest + "\\" + OE_name)
