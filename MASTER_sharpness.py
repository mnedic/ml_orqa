# Description:
# Estimation of image sharpness. A slice is taken across middle of the gradient of imaged checkered pattern
# Threshold for peak detection depends on noise and mean of the slice. Blurry images give higher mean values and larger standard deviations --> this eliminates lower peaks. Images that are sharp enough have all the peaks detected.
# From checkered image name we find details of the pattern and estimate expected number of peaks.
# If number of expected peaks does not match number of detected peaks, an error flag is raised.

# IMPORTANT: Put this .py file next to ACQUISITION folder. Same directory should contain image_analysis_module.py

# Modules import
import os
from os import listdir
from os.path import isfile, join
import sys
import warnings

import image_analysis_module

import time
start_time = time.time()

if not sys.warnoptions:
    warnings.simplefilter("ignore")

print("\n****************************** MASTER_sharpness ******************************")
print("******************************    Start of code.    ****************************")
# Change directory to ACQUISITION folder. Find the OE directory that needs to be analyzed
dir_path = os.path.dirname(os.path.realpath(__file__))
acquisition_path = dir_path + "\\ACQUISITION"
os.chdir(acquisition_path)

OE_name = str(sys.argv[1])
OE_dir = [f for f in listdir(acquisition_path) if f == OE_name]

if(len(OE_dir) != 1):
    # print(OE_name, OE_dir)
    print("{} not found!".format(OE_name))
    quit()

# Iterate current test for all eligible images found in current OE
temp_dir = acquisition_path + "\\" + OE_name + "\\ScreenImages"
# print(temp_dir)
onlyfiles = [f for f in listdir(temp_dir) if isfile(join(temp_dir, f))]

# Create new folder for results
myfolder = "ResultSharpness"
image_analysis_module.mkdir_analysis(acquisition_path + "\\" + OE_name + '\\' + myfolder)
os.chdir(acquisition_path + "\\" + OE_name + '\\' + myfolder)

# Import images
paths = image_analysis_module.import_display_images(temp_dir)
filename = os.path.splitext(os.path.basename(paths[0]))[0]

# Load reference image - white screen
mask, mask_crop, readErr = image_analysis_module.otsu_cut_and_mask(paths[0])

json_feedback = []
if readErr:
    json_feedback.append(image_analysis_module.json_feedback(filename, 2, 7))
    image_analysis_module.json_write_report(dir_path, json_feedback)
    print("readErr occured!")
    quit()

# Loop analysis over all images that meet the requirement (checkers)
i = 0
for path in paths:
    if not ("lpmm" in path) or ("white" in path):
        continue

    print('\n{} in {} START....................................................'.format(i+1, OE_name))
    # Apply mask and send for sharpness estimation
    filename = os.path.splitext(os.path.basename(path))[0]
    print(filename)

    image_crop, masked_image, masked_image_gray, readErr = image_analysis_module.apply_mask(path, mask, mask_crop)

    if readErr:
        json_feedback.append(image_analysis_module.json_feedback(filename, 2, 7))
        image_analysis_module.json_write_report(dir_path, json_feedback)
        print("readErr occured!")
        quit()

    warning = image_analysis_module.magic_flip_test(masked_image_gray, mask, filename, path, acquisition_path + "\\" + OE_name + '\\' + myfolder)

"""
    if warning:
        json_feedback.append(image_analysis_module.json_feedback(filename, 2, 4))
        print("FAIL")
    else:
        json_feedback.append(image_analysis_module.json_feedback(filename, 2, 1))
        print("PASS!")
"""
print('{} in {} DONE. Time....{}.............................'.format(i+1, OE_name, time.time() - start_time))
i += 1

image_analysis_module.json_write_report(dir_path, json_feedback)

os.chdir(dir_path)
print("\n****************************** MASTER_sharpness ******************************")
print("******************************    End of code.    ****************************")
print("RUN TIME: %s seconds\n" % (time.time() - start_time))
