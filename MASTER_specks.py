# Description:
# White and R, G, B images go through canny edge detection. Besides the display edge itself, this also detects prominent specks in the image. Blob detection indentifies these specks, and should there be any, raises an error flag.

# IMPORTANT: Put this .py file next to ACQUISITION folder. Same directory should contain image_analysis_module.py

# Modules import
import os
from os import listdir
from os.path import isfile, join
import sys
import warnings
from matplotlib import pyplot as plt

import image_analysis_module

import time
start_time = time.time()

if not sys.warnoptions:
    warnings.simplefilter("ignore")

print("\n****************************** MASTER_specks ******************************")
print("*******************************  Start of code. *****************************")
# Change directory to ACQUISITION folder. Find the OE directory that needs to be analyzed
dir_path = os.path.dirname(os.path.realpath(__file__))
acquisition_path = dir_path + "\\ACQUISITION"
os.chdir(acquisition_path)

OE_name = str(sys.argv[1])
OE_dir = [f for f in listdir(acquisition_path) if f == OE_name]

if(len(OE_dir) != 1):
    # print(OE_name, OE_dir)
    print("{} not found!".format(OE_name))
    quit()

# Iterate current test for all eligible images found in current OE
temp_dir = acquisition_path + "\\" + OE_name + "\\ScreenImages"
onlyfiles = [f for f in listdir(temp_dir) if isfile(join(temp_dir, f))]

# Create new folder for results
myfolder = "RESULTS Specs"
image_analysis_module.mkdir_analysis(acquisition_path + "\\" + OE_name + '\\' + myfolder)
os.chdir(acquisition_path + "\\" + OE_name + '\\' + myfolder)

# Import images
paths = image_analysis_module.import_display_images(temp_dir)
filename = os.path.splitext(os.path.basename(paths[0]))[0]

# Load reference image - white screen
mask, mask_crop, readErr = image_analysis_module.otsu_cut_and_mask(paths[0])
json_feedback = []
if readErr:
    json_feedback.append(image_analysis_module.json_feedback(filename, 3, 7))
    image_analysis_module.json_write_report(dir_path, json_feedback)
    print("readErr occured!")
    quit()

# Loop analysis over all images that meet the requirement (white and R,G,B)
i = 0
for path in paths:

    if not (("white" in path)):
        continue

    print('\n{} in {} START....................................................'.format(i+1, OE_name))
    # Cut image, send for edge detection and then check for prominent specks
    filename = os.path.splitext(os.path.basename(path))[0]

    # print(filename)
    check_specks, readErr = image_analysis_module.cut_image(path, filename)
    if readErr:
        json_feedback.append(image_analysis_module.json_feedback(filename, 3, 7))
        image_analysis_module.json_write_report(dir_path, json_feedback)
        print("readErr occured!")
        quit()

    if check_specks:
        json_feedback.append(image_analysis_module.json_feedback(filename, 3, 5))
        # print("Possible foreign particles in lens system. Specks test failed!")
    else:
        json_feedback.append(image_analysis_module.json_feedback(filename, 3, 1))
        # print("PASS!")

    print('{} in {} DONE. Time....{}.............................'.format(i+1, OE_name, time.time() - start_time))
    i += 1

image_analysis_module.json_write_report(dir_path, json_feedback)

os.chdir(dir_path)
print("\n****************************** MASTER_specks ******************************")
print("*******************************   End of code.  *****************************")
print("RUN TIME: %s seconds\n" % (time.time() - start_time))
