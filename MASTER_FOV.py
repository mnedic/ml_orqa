# Description:
# Calculate field of view from white test image of analyzed OEs
# IMPORTANT: Put this .py file next to ACQUISITION folder. Same directory should contain image_analysis_module.py

# Modules import
import os
from skimage.util import invert
from os.path import isfile, join
import sys
import warnings

import image_analysis_module

import time
start_time = time.time()

if not sys.warnoptions:
    warnings.simplefilter("ignore")

print("\n****************************** MASTER_FOV ******************************")
print("****************************** Start of code. ****************************")
# print("START TIME: %s seconds\n" % (time.time() - start_time))

# Change directory to ACQUISITION folder. Find the OE directory that needs to be analyzed
dir_path = os.path.dirname(os.path.realpath(__file__))
acquisition_path = dir_path + "\\ACQUISITION"
os.chdir(acquisition_path)

OE_name = str(sys.argv[1])
OE_dir = [f for f in os.listdir(acquisition_path) if f == OE_name]

if(len(OE_dir) != 1):
    print("{} not found!".format(OE_name))
    quit()

# Iterate current test for all eligible images found in current OE
temp_dir = acquisition_path + "\\" + OE_name + "\\ScreenImages"
onlyfiles = [f for f in os.listdir(temp_dir) if isfile(join(temp_dir, f))]

# Create new folder for results
myfolder = "RESULTS FOV"
image_analysis_module.mkdir_analysis(acquisition_path + "\\" + OE_name + '\\' + myfolder)
os.chdir(acquisition_path + "\\" + OE_name + '\\' + myfolder)

# Import images
paths = image_analysis_module.import_display_images(temp_dir)
# filenames = os.path.splitext(os.path.basename(paths[0]))[0]

# Loop analysis over all images that meet the requirement (white)
i = 0
for path in paths:
    if not ("white" in path):
        continue

    print('\n{} in {} START....................................................'.format(i+1, OE_name))

    # Call function "fov_calculator" to calculate FOV of a display from acquired image of white screen
    filename = os.path.splitext(os.path.basename(path))[0]

    mask, mask_crop, readErr = image_analysis_module.otsu_cut_and_mask(path)
    json_feedback = []
    if readErr:
        json_feedback.append(image_analysis_module.json_feedback(filename, 1, 7))
        image_analysis_module.json_write_report(dir_path, json_feedback)
        print("readErr occured!")
        quit()

    error_unreliable, check, readErr = image_analysis_module.fov_calculator(path, invert(mask), acquisition_path + "\\" + OE_name + '\\' + myfolder)


    if readErr:
        json_feedback.append(image_analysis_module.json_feedback(filename, 1, 7))
        image_analysis_module.json_write_report(dir_path, json_feedback)
        print("readErr occured!")
        quit()

    if error_unreliable:
        json_feedback.append(image_analysis_module.json_feedback(filename, 1, 2))
        print("WARNING! Peak detection failed, results unreliable.")
        # print("Image too noisy for FOV calculation! FOV test failed!")
    elif check:
        json_feedback.append(image_analysis_module.json_feedback(filename, 1, 3))
        print("FOV outside of expected range! FOV test failed!")
    else:
        json_feedback.append(image_analysis_module.json_feedback(filename, 1, 1))
        print("PASS!")

    print('{} in {} DONE. Time....{}.............................'.format(i+1, OE_name, time.time() - start_time))
    i += 1
    image_analysis_module.json_write_report(dir_path, json_feedback)

os.chdir(dir_path)
print("\n****************************** MASTER_FOV ******************************")
print("******************************* End of code. *****************************")
print("RUN TIME: %s seconds\n" % (time.time() - start_time))
