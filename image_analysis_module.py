# Image Analysis Module (IAM) contains:
# 1. mkdir_analysis()
# 2. import_display_images()
# 3. otsu_cut_and_mask()
# 4. json_write_report()
# 5. json_feedback()
# 6. fov_calculator() --> MASTER_FOV
# 7. apply_mask()
# 8. magic_flip_test() --> MASTER_flip-test
# 8a. transform_coordinates()
# 8b. f_D()
# 8c. distortion()
# 9. cut_image() --> MASTER_specks
# 10. intensity_check() --> MASTER_intensity

# *11. flare_check() --> izdvojeni test, ne koriste MASTERi automatskog QC testa

# X. detect_and_color_edge()
# X. check_for_spots()

import imageio
from cv2 import boundingRect

import numpy as np
from matplotlib import pyplot as plt
from math import sqrt

from scipy import ndimage
from scipy.signal import find_peaks

import os
from os.path import join

from skimage.color import rgb2gray
from skimage import img_as_ubyte, img_as_uint, io
from skimage import exposure, filters
from skimage.filters import threshold_otsu
from skimage.util import invert
from skimage import feature
from skimage.feature import blob_dog
from skimage.measure import label, regionprops
from skimage.morphology import disk
from skimage import morphology
 

from datetime import datetime
import time
start_time = time.time()

# 1
def mkdir_analysis(new_path):
    os.makedirs(new_path, exist_ok=True)

# 2
def import_display_images(mypath):
    onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(join(mypath, f))]

    paths = []
    for filepath in onlyfiles:
        paths.append(mypath + '\\' + filepath)

    return paths

# 3
def otsu_cut_and_mask(path):
    # Try-Except is applied for white image here (as first acquired image it is most susceptible to truncation due to limitations of cameras)
    try:
        im = imageio.imread(path)
    except ValueError:
        print("Unable to read the image! File missing or truncated.")
        return None, None, 1

    # Pretvorba slike (1) u greyscale
    #io.imshow(im)
    #io.show()
    im_g = rgb2gray(im)
    img = img_as_ubyte(im_g)

    # Uvođenje blura da se šum nakon otsu tehnike ne uvede u masku
    img = ndimage.gaussian_filter(img, 1)

    # Postavke za Otsu Threshholding
    threshold_global_otsu = threshold_otsu(img)
    global_otsu = img >= threshold_global_otsu

    # Masking:
    mask = global_otsu  # < 0.2
    mask = invert(mask)  # Maska se mora invertirati prije primjene na slici jer se slika mijenja samo gdje je maska True - bijela


    # Cropanje maske: find where the signature is and make a cropped region
    points = np.argwhere(mask == 0)  # find where the black pixels are
    points = np.fliplr(points)  # store them in x,y coordinates instead of row,col indices
    x, y, w, h = boundingRect(points)  # create a rectangle around those points
    x, y, w, h = x-30, y-30, w+70, h+70  # make the box a little bigger
    mask_crop = mask.copy()
    mask_crop = mask_crop[y:y+h, x:x+w]  # create a cropped region of the gray image

    #if (mask_crop.shape[0]>1000 and mask_crop.shape[1]>2000):     #za slike veće rezulucije 
    if (mask_crop.shape[0] > 500 and mask_crop.shape[1] > 1000):   # za slike manje rezulucije
        #io.imshow(mask)
        #io.show()
        return mask, mask_crop, 0
    else:
        labeled_mask = label(mask, background=5)
        clusters = regionprops(labeled_mask, img)

        # Sort clusters after descending area size - first is background, second is display, rest is noise
        area_list = []
        for cluster in clusters:
            area_list.append(cluster.area)

        area_tuple = [(cluster.area, cluster.label) for cluster in clusters]
        sorted_area_tuple = sorted(area_tuple, reverse=True)
        background = clusters[sorted_area_tuple[0][1]-1]
        display = clusters[sorted_area_tuple[1][1]-1]

        mask = np.ones((im.shape[0], im.shape[1]))
        for i, j in display.coords:
            mask[i, j] = 0

        # # Cropanje maske: find where the signature is and make a cropped region
        points = display.coords
        points = np.fliplr(points)  # store them in x,y coordinates instead of row,col indices
        x, y, w, h = boundingRect(points)  # create a rectangle around those points
        x, y, w, h = x-30, y-30, w+70, h+70  # make the box a little bigger
        mask_crop = mask.copy()
        mask_crop = mask_crop[y:y+h, x:x+w]  # create a cropped region of the gray image

        io.imshow(mask)
        io.show()
        return mask, mask_crop, 0

# 4
def json_write_report(dir_path, json_feedback):
    # Json Report
    warning_path = dir_path + '\\ACQUISITION\\OE_TEST_RESULTS.txt'
    if(os.path.exists(warning_path)):
        text_file = open(warning_path, 'r+')
        text_file.seek(0, os.SEEK_END)              # seek to end of file; f.seek(0, 2) is legal
        text_file.seek(text_file.tell() - 3, os.SEEK_SET)   # go backwards 3 bytes
        text_file.write(",\n")
    else:
        text_file = open(warning_path, 'w')
        text_file.write("[")

    for i in range(len(json_feedback)):
        if(i+1 == len(json_feedback)):
            text_file.write("{}]\n".format(json_feedback[i]))
        else:
            text_file.write("{},\n".format(json_feedback[i]))

    text_file.close()

# 5
def json_feedback(filename, test, flag_id):
    start = "OE"

    oe_name = filename[filename.find(start):]

    start = "_"
    end = "_OE"
    testImage = filename[filename.find(start)+1:filename.rfind(end)]

    t = datetime.today()
    date = "{}.{}.{}".format(t.day, t.month, t.year)
    time = "{:02}:{:02}:{:02}".format(t.hour, t.minute, t.second)

    json_name = "\"name\": \"{}\"".format(oe_name)
    json_test = "\"test\": \"{}\"".format(test)
    json_testImg = "\"testImg\": \"{}\"".format(testImage)
    json_flag = "\"flagID\": \"{}\"".format(flag_id)
    json_datetime = "\"datetime\": \"{} {}\"".format(date, time)

    json = "{{ {}, {}, {}, {}, {} }}".format(json_name, json_test, json_flag, json_datetime, json_testImg)

    return json

# 6
def fov_calculator(path, mask, report_path):
    filename = os.path.splitext(os.path.basename(path))[0]

    t = datetime.today()
    date = "{}.{}.{}".format(t.day, t.month, t.year)
    time = "{:02}:{:02}:{:02}".format(t.hour, t.minute, t.second)

    print(filename)
    try:
        image = imageio.imread(path)
    except ValueError:
        print("Unable to read the image! File missing or truncated.")
        return None, None, 1

    image = rgb2gray(image)

    labeled_mask = label(mask, background=5)
    clusters = regionprops(labeled_mask, image)

    # Sort clusters after descending area size - first is background, second is display, rest is noise
    area_list = []
    for cluster in clusters:
        area_list.append(cluster.area)

    area_tuple = [(cluster.area, cluster.label) for cluster in clusters]
    sorted_area_tuple = sorted(area_tuple, reverse=True)
    background = clusters[sorted_area_tuple[0][1]-1]
    display = clusters[sorted_area_tuple[1][1]-1]

    test = mask
    coords = feature.corner_peaks(feature.corner_harris(test, k=0, sigma=5), min_distance=500, num_peaks=4)

    if(len(coords) != 4):
        print("WARNING! Peak detection failed, results unreliable.")
        # print(len(coords))
        report_path += '\\REPORT - FOV.txt'
        text_file = open(report_path, 'w')
        text_file.write("{} @ {}:\n".format(date, time))
        text_file.write("REPORT - FOV\n")
        text_file.write("{}\n".format(filename))
        text_file.write("WARNING! Peak detection failed, results unreliable.\n")
        text_file.write("FOV is not calculated.")
        text_file.close()

        return True, None, 0


    coords_tuple = [(coord[0], coord[1]) for coord in coords]

    from operator import itemgetter
    sorted_coords_tuple = sorted(coords_tuple, reverse=False, key=itemgetter(0))

    UP = sorted_coords_tuple[0:2]
    DOWN = sorted_coords_tuple[2:4]

    LU, RU = sorted(UP, reverse=False, key=itemgetter(1))
    LD, RD = sorted(DOWN, reverse=False, key=itemgetter(1))

    # fig, ax = plt.subplots()
    # # ax.imshow(feature.shape_index(test), cmap=plt.cm.gray)
    # ax.imshow(image, cmap=plt.cm.gray)
    # # ax.imshow(test, cmap=plt.cm.gray)
    # # ax.plot(coords_subpix[:, 1], coords_subpix[:, 0], '+r', markersize=15)
    # # ax.axis((0, 310, 200, 0))
    # minr, minc, maxr, maxc = display.bbox
    # bx = (minc, maxc, maxc, minc, minc)
    # by = (minr, minr, maxr, maxr, minr)
    # ax.plot(bx, by, '-b', linewidth=2.5)
    # coords = [LU, RU, LD, RD]
    # coords = np.asarray(coords)
    # color = ['r', 'g', 'cyan', 'magenta']
    # for i in range(0,4):
    #     ax.plot(coords[i, 1], coords[i, 0], marker='o', color=color[i], linestyle='None', markersize=6)
    #     # ax.plot(coords[:, 1], coords[:, 0], color='cyan', marker='o', linestyle='None', markersize=6)
    # plt.savefig('FOV-{}.png'.format(os.path.splitext(os.path.basename(path))[0]), bbox_inches='tight')
    # # plt.show()
    # plt.close()

    d1 = sqrt((RD[1]-LU[1])**2 + (RD[0]-LU[0])**2)
    d2 = sqrt((RU[1]-LD[1])**2 + (RU[0]-LD[0])**2)
    d_peaks = []
    d_peaks.append(d1)
    d_peaks.append(d2)

    H = background.bbox[2]-background.bbox[0]
    W = background.bbox[3]-background.bbox[1]
    D = sqrt(H**2+W**2)

    # Calibration factor calculated from the difference between sensor format and lens design format and lens designed FOV
    # OLD:  imaging_FOV = 95.3768
    # CAM DISTORTION NOT TAKEN INTO ACCOUNT!
    imaging_FOV = 94.2012515501
    display_FOV = imaging_FOV*np.mean(d_peaks)/D

    results = [d_peaks[0]*imaging_FOV/D, d_peaks[1]*imaging_FOV/D, np.mean(d_peaks)*imaging_FOV/D]

    # Setting upper limit for acceptable FOV as +-1° of the designed value
    designed = 44  # but this is not observed correctly due to barell distortion from the camera itself --> imFOV set to fit new set of images to mean 44°

    upper_limit = 45
    lower_limit = 43

    results_bool = []
    for result in results:
        if ((result < upper_limit)  and (result > lower_limit)):
            results_bool.append("PASS")
        else:
            results_bool.append("FAIL")

    # print(results_bool)
    # print(results_bool.count("PASS"))

    if(results_bool.count("PASS") != 3):
        display_FOV = (np.asarray(results)).max()
        # print(display_FOV)

    if (display_FOV > upper_limit or display_FOV < lower_limit):
        check = True
    else:
        check = False

    print("OE FOV:\t{}°".format(display_FOV))



    # Create report
    report_path += '\\REPORT - FOV.txt'
    text_file = open(report_path, 'w')
    text_file.write("{} @ {}:\n".format(date, time))
    text_file.write("REPORT - FOV\n")
    # text_file.write("FOV(Imaging system): {}°\n".format(imaging_FOV))
    text_file.write("{}: {:.4f}°\n".format(filename, display_FOV))

    if(check):
        text_file.write("FAIL")
    else:
        text_file.write("PASS")

    text_file.close()


    return False, check, 0


# 7
def apply_mask(path, mask, mask_crop):
    try:
        image = imageio.imread(path)
    except ValueError:
        print("Unable to read the image! File missing or truncated.")
        return None, None, None, 1


    # Cropanje slike koja će ići na analizu, da se ubrza provedba koda
    # find where the signature is and make a cropped region
    points = np.argwhere(mask==0) # find where the black pixels are
    points = np.fliplr(points) # store them in x,y coordinates instead of row,col indices
    x, y, w, h = boundingRect(points) # create a rectangle around those points
    x, y, w, h = x-30, y-30, w+70, h+70 # make the box a little bigger
    image_crop = image.copy()
    image_crop = image_crop[y:y+h, x:x+w] # create a cropped region of the gray image

    # Primjena maske
    masked_image = image_crop.copy()
    masked_image[mask_crop>0] = 0
    plt.imshow(masked_image, cmap=plt.cm.gray)
    plt.show()
    # Masked image in grayscale
    masked_image_gray = rgb2gray(masked_image)

    plt.imshow(masked_image_gray, cmap=plt.cm.gray)
    plt.show()
    return image_crop, masked_image, masked_image_gray, 0


# 8
def magic_flip_test(masked_image_gray, mask, filename, path, report_path):
    image = masked_image_gray
    plt.imshow(image, cmap=plt.cm.gray)
    plt.show()
    image = rgb2gray(image)
    image = img_as_ubyte(image)
    plt.imshow(image, cmap=plt.cm.gray)
    plt.show()

    # Field1 = 0 mm
    tH1, tW1 = transform_coordinates((0,0), from_type="relative_display", to_type="absolute_pxl", full_size_image_shape=image.shape)
    # print("tH0 = {}, tW0 = {}".format(tH1, tW1))

    # Field5 = 6.2 mm
    search_back = 0
    for i in range(len(image[int(tH1), :])):
        if(image[int(tH1), i] != 0 and not search_back):
            edge_front = i
            search_back = 1
        elif(image[int(tH1), i] == 0 and search_back):
            edge_back = i-1
            break
        else:
            continue

    # Field2 = 3.5 mm
    _, tW2 = transform_coordinates((0,edge_front), from_type="absolute_pxl", to_type="relative_pxl", full_size_image_shape=image.shape)
    tW2 = 3.5/6.2 * tW2
    _, tW2 = transform_coordinates((int(tW2)), from_type="relative_pxl", to_type="absolute_pxl", full_size_image_shape=image.shape)


    _, tW2_back = transform_coordinates((0,edge_back), from_type="absolute_pxl", to_type="relative_pxl", full_size_image_shape=image.shape)
    tW2_back = 3.5/6.2 * tW2_back
    _, tW2_back = transform_coordinates((int(tW2_back)), from_type="relative_pxl", to_type="absolute_pxl", full_size_image_shape=image.shape)

    # # Field3 = 4 mm
    _, tW3 = transform_coordinates((0,edge_front), from_type="absolute_pxl", to_type="relative_pxl", full_size_image_shape=image.shape)
    tW3 = 4/6.2 * tW3
    _, tW3 = transform_coordinates((int(tW3)), from_type="relative_pxl", to_type="absolute_pxl", full_size_image_shape=image.shape)

    _, tW3_back = transform_coordinates((0,edge_back), from_type="absolute_pxl", to_type="relative_pxl", full_size_image_shape=image.shape)
    tW3_back = 4/6.2 * tW3_back
    _, tW3_back = transform_coordinates((int(tW3_back)), from_type="relative_pxl", to_type="absolute_pxl", full_size_image_shape=image.shape)

    # # Field4 = 5 mm
    _, tW4 = transform_coordinates((0,edge_front), from_type="absolute_pxl", to_type="relative_pxl", full_size_image_shape=image.shape)
    tW4 = 5/6.2 * tW4
    _, tW4 = transform_coordinates((int(tW4)), from_type="relative_pxl", to_type="absolute_pxl", full_size_image_shape=image.shape)

    _, tW4_back = transform_coordinates((0,edge_back), from_type="absolute_pxl", to_type="relative_pxl", full_size_image_shape=image.shape)
    tW4_back = 5/6.2 * tW4_back
    _, tW4_back = transform_coordinates((int(tW4_back)), from_type="relative_pxl", to_type="absolute_pxl", full_size_image_shape=image.shape)

    # CHECK THE POSITIONS OF SIMULATED FIELDS AND IMAGE SLICES
    # plt.imshow(image, cmap=plt.cm.gray)
    # plt.vlines(tW1, linestyle=':', ymin=0, ymax=image.shape[0]-10, color='red')
    # plt.hlines(tH1, linestyle=':', xmin=0, xmax=image.shape[1]-10, color='red')
    # plt.vlines(edge_front, linestyle=':', ymin=0, ymax=image.shape[0]-10, color='limegreen')
    # plt.vlines(edge_back, linestyle=':', ymin=0, ymax=image.shape[0]-10, color='limegreen')
    # plt.vlines(tW2, linestyle=':', ymin=0, ymax=image.shape[0]-10, color='cyan')
    # plt.vlines(tW2_back, linestyle=':', ymin=0, ymax=image.shape[0]-10, color='cyan')
    # plt.vlines(tW3, linestyle=':', ymin=0, ymax=image.shape[0]-10, color='darkorange')
    # plt.vlines(tW3_back, linestyle=':', ymin=0, ymax=image.shape[0]-10, color='darkorange')
    # plt.vlines(tW4, linestyle=':', ymin=0, ymax=image.shape[0]-10, color='magenta')
    # plt.vlines(tW4_back, linestyle=':', ymin=0, ymax=image.shape[0]-10, color='magenta')
    #
    # plt.show()

    gnorm_mask = image
    slice = gnorm_mask[int(tH1), :]
    peaks,_ = find_peaks(invert(slice), distance=5)
    peaks_down = [peak for peak in peaks if slice[peak] < 25]
    #peaks2, _ = find_peaks(slice, distance=5)
    x = range(0, slice.shape[0])
    plt.plot(slice)
    plt.plot(peaks_down, slice[peaks_down], "x", color='red')
    #plt.plot(peaks2, slice[peaks2], "x", color='red')
    plt.savefig('image_slice_lpmm', bbox_inches='tight')
    #plt.savefig('image_slice_lpmm_bothSide.png', bbox_inches='tight')
    #plt.show()
    return 
    
    
  
    peaks_mean = np.mean(slice[peaks])
    peaks_std = np.std(slice[peaks])


    acceptable_peaks = [maximum_id for maximum_id in peaks if (slice[maximum_id] < peaks_mean+3*peaks_std and slice[maximum_id]<150)]

    

    # Linear regression
    from scipy.stats import linregress
    slope, intercept, r_value, p_value, std_err = linregress(acceptable_peaks, slice[acceptable_peaks])

    # plotanje ponasanja - za potrebe razvoja algoritma za detekciju flipa
    plt.figure(figsize=(16, 9))
    # plt.axis(ymax=0.2)

    xmin = edge_front-10
    xmax = edge_back+10
    # xmin = 0
    # xmax = 528
    # print("xmin, xmax = ", xmin, xmax)

    plt.axis(ymin=0)
    plt.axis(ymax=256)
    # plt.axis(xmin=0, xmax=3450)
    plt.axis(xmin=xmin, xmax=xmax)

    # if (lpmm == "12"):
    #     plt.suptitle("{} lp/mm".format(12.6))
    # elif (lpmm == "15"):
    #     plt.suptitle("{} lp/mm".format(15.78))

    # plt.scatter(peaks, slice[peaks])
    plt.plot(peaks, slice[peaks], "x", color='red')
    # plt.plot(iqr_acceptable_peaks, slice[iqr_acceptable_peaks], "o", color='blue')
    plt.plot(acceptable_peaks, slice[acceptable_peaks], "*", color='Lime')

    # plt.hlines(peaks_mean, x[0], x[-1], color='red', linestyle='-')
    # plt.hlines(peaks_mean+3*peaks_std, x[0], x[-1], color='red', linestyle='--')
    # plt.hlines(peaks_mean-3*peaks_std, x[0], x[-1], color='red', linestyle='--')
    # plt.hlines(tH1, linestyle=':', xmin=xmin, xmax=xmax, color='red')
    # plt.hlines(peaks_median, x[0], x[-1], color='blue',  linestyle='-')
    # plt.hlines(q25, x[0], x[-1], color='blue', linestyle='--')
    # plt.hlines(lower, x[0], x[-1], color='blue', linestyle='-.')
    # plt.hlines(q75, x[0], x[-1], color='blue', linestyle='--')
    # plt.hlines(upper, x[0], x[-1], color='blue', linestyle='-.')
    # # plt.hlines(slice_mean, x[0], x[-1], color='red')
    # plt.hlines(slice_mean-3*slice_std, x[0], x[-1], color='black', linestyle='--')
    # plt.hlines(slice_median, x[0], x[-1], color='red',  linestyle='-')
    # plt.hlines(sq75, x[0], x[-1], color='red', linestyle='--')
    # plt.hlines(supper, x[0], x[-1], color='red', linestyle='-.')
    # plt.hlines(sq25, x[0], x[-1], color='red', linestyle='--')
    # plt.hlines(slower, x[0], x[-1], color='DarkGreen', linestyle='-.')

    plt.vlines(tW1, linestyle=':', ymin=0, ymax=256, color='red')
    plt.vlines(edge_front, linestyle=':', ymin=0, ymax=256, color='limegreen')
    plt.vlines(edge_back, linestyle=':', ymin=0, ymax=256, color='limegreen')
    plt.vlines(tW2, linestyle=':', ymin=0, ymax=256, color='cyan')
    plt.vlines(tW2_back, linestyle=':', ymin=0, ymax=256, color='cyan')
    plt.vlines(tW3, linestyle=':', ymin=0, ymax=256, color='darkorange')
    plt.vlines(tW3_back, linestyle=':', ymin=0, ymax=256, color='darkorange')
    plt.vlines(tW4, linestyle=':', ymin=0, ymax=256, color='magenta')
    plt.vlines(tW4_back, linestyle=':', ymin=0, ymax=256, color='magenta')

    plt.hlines(slice[acceptable_peaks].max(), x[0], x[-1], color='m', linestyle='-')
    # plt.hlines(slice[acceptable_peaks].min(), x[0], x[-1], color='m', linestyle='-')


    # plt.hlines(peaks_mean, x[0], x[-1], color='blue')
    # plt.hlines(peaks_mean+2*peaks_std, x[0], x[-1], color='blue', linestyle=':')
    # plt.hlines(peaks_mean-2*peaks_std, x[0], x[-1], color='blue', linestyle=':')


    # linear regression - slope, intercept, r_value, p_value, std_err
    acc_full = linregress(acceptable_peaks, slice[acceptable_peaks])
    # plt.plot(peaks, acc_full[0]*peaks+acc_full[1])
    #
    half1_acceptable_peaks = [p for p in acceptable_peaks if p < tW1]
    acc_half1 = linregress(half1_acceptable_peaks, slice[half1_acceptable_peaks])
    # plt.plot(peaks, acc_half1[0]*peaks+acc_half1[1])
    #
    half2_acceptable_peaks = [p for p in acceptable_peaks if p > tW1]
    acc_half2 = linregress(half2_acceptable_peaks, slice[half2_acceptable_peaks])
    # plt.plot(peaks, acc_half2[0]*peaks+acc_half2[1])
    #
    #
    # plt.show()
    plt.savefig('image_slice_lpmm-{}.png'.format(filename[2:]), bbox_inches='tight')
    # plt.savefig('image_slice_line-{}.png'.format(filename), bbox_inches='tight')
    plt.close()

    # FLIP TESTS
    # acc-stats: 0-slope, 1-intercept, 2-r_value, 3-p_value, 4-std_err
    # flip_test_results = ["", "", "", ""]

    # TEST 1: Max
    # if(slice[acceptable_peaks].max() > 100):
    #if(slice[acceptable_peaks].max() > 70):
     #   flip_test_results[0] = "NORM"
    #else:
    #    flip_test_results[0] = "MF"

    # TEST 2: Slope
    #if(acc_half1[0] > 0.02 and acc_half2[0] < -0.02 and (acc_full[0] <= 0.01 and acc_full[0] >= -0.01)):
    #    flip_test_results[1] = "NORM"
    #elif((acc_full[0] <= 0.15 and acc_full[0] >= -0.15) and (acc_half1[0] <= 0.15 and acc_half1[0] >= -0.15) and (acc_half2[0] <= 0.15 and acc_half2[0] >= -0.15)):
    #    flip_test_results[1] = "MF"
    #else:
    #    flip_test_results[1] = None

    # TEST 3: Intercept
    #if(acc_half2[1] > 90):
    #    flip_test_results[2] = "NORM"
    #elif(acc_half2[1] < 90):
    #    flip_test_results[2] = "MF"

    # TEST 4: R (correlation coefficient)
    #if(acc_half1[2] > 0.75 and acc_half2[2] < -0.75):
    #    flip_test_results[3] = "NORM"
    #elif(acc_half1[2] < 0.75 and acc_half2[2] > -0.75):
    #    flip_test_results[3] = "MF"


    #print(flip_test_results)

    #t = datetime.today()
    #date = "{}.{}.{}".format(t.day, t.month, t.year)
    #time = "{:02}:{:02}:{:02}".format(t.hour, t.minute, t.second)

    # Create report
    #report_path += '\\REPORT - MAGIC FLIP.txt'
    #text_file = open(report_path, 'a')
    #text_file.write("{} @ {}:\n".format(date, time))
    #text_file.write("REPORT - MAGIC FLIP\n")
    #text_file.write("(NORM = standard orientation, MF = middle lens flipped)\n")
    #text_file.write("{}\n".format(filename))
    #text_file.write("Results of 4-part orientation test:\n [max, slope, intercept, R] = {}\n".format(flip_test_results))
	
    #if flip_test_results.count(flip_test_results[0]) != len(flip_test_results):
    #    print("Orientation of the middle lens cannot be determined. FAIL")
    #    print("Test again (aquire new image for analysis). If the same result occurs, open the engine for manual check.")
    #    flag = True
    #    text_file.write("Orientation of the middle lens cannot be determined.\nTest again (aquire new image for analysis). If the same result occurs, open the engine for manual check.\n\nFAIL")
    #elif(flip_test_results[0] == "NORM"):
        # print("PASS")
    #    text_file.write("PASS\n\n")
    #    flag = False
    #elif(flip_test_results[0] == "MF"):
        # print("FAIL")
    #    text_file.write("FAIL\n\n")
    #    flag = True

    #text_file.close()


    return flag


# 8a
def transform_coordinates(coord, from_type, to_type, full_size_image_shape):
    height, width = full_size_image_shape
    # print(height, width)
    pxl_pitch = 0.0078  # mm
    # print("Transforming coordinates from {} to {}.".format(from_type, to_type))
    if(from_type == 'absolute_pxl' and to_type == 'relative_pxl'):
        [xr, yr] = np.subtract(coord, [height/2, width/2])
        # print("Absolute pixel coordinates = {}\nRelative pixels coordinates = {}\n".format(coord, [xr, yr]))
        return xr, yr

    if(from_type == 'absolute_pxl' and to_type == 'relative_display'):
        [xr, yr] = np.subtract(coord, [height/2, width/2])
        [xrD, yrD] = np.multiply([xr, yr], pxl_pitch)
        # print("Absolute pixel coordinates = {}\nRelative pixels coordinates = {}\nRelative display coordinates = {}\n".format(coord, [xr, yr], [xrD, yrD]))
        # print("\ncoord = {}\n[h/2, w/2] = {}\nrelative = {}".format(coord, [height/2, width/2], [xr, yr]))
        return xrD, yrD

    if(from_type == 'relative_display' and to_type == 'relative_pxl'):
        [xr, yr] = np.multiply(coord, 1/pxl_pitch)
        # print("Relative pixels coordinates = {}\nRelative display coordinates = {}\n".format([xr, yr], coord))
        return xr, yr

    if(from_type == 'relative_display' and to_type == 'absolute_pxl'):
        [xr, yr] = np.multiply(coord, 1/pxl_pitch)
        [x, y] = np.subtract([xr, yr], [-height/2, -width/2])
        # print("Absolute pixel coordinates = {}\nRelative pixels coordinates = {}\nRelative display coordinates = {}\n".format([x, y], [xr, yr], coord))
        return x, y

    if(from_type == 'relative_pxl' and to_type == 'absolute_pxl'):
        [x, y] = np.subtract(coord, [-height/2, -width/2])
        # print("Absolute pixel coordinates = {}\nRelative pixels coordinates = {}\n".format([x, y], coord))
        return x, y

    if(from_type == 'relative_pxl' and to_type == 'relative_display'):
        [xrD, yrD] = np.multiply(coord, pxl_pitch)
        # print("Relative pixels coordinates = {}\nRelative display coordinates = {}\n".format(coord, [xrD, yrD]))
        return xrD, yrD

# 8b
def f_D(t):
    a2 = 0.2477  # PINCUSHION
    return a2*t**2

# 8c
# Square fit of distortion dataset: a2*x_ref**2
def distortion(x_ref, y_ref, full_size_image_shape):
    pxl_pitch = 0.0078  # mm

    [x_ref, y_ref] = transform_coordinates([x_ref, y_ref], 'absolute_pxl', 'relative_display',full_size_image_shape)

    r_ref = sqrt(x_ref**2 + y_ref**2)
    r = r_ref*(1+f_D(r_ref)/100)

    if(x_ref == 0):
        x = 0
        y = r*np.sign(y_ref)
    else:
        x = np.sign(x_ref)*r/sqrt(1+(y_ref/x_ref)**2)
        y = y_ref/x_ref * x

    [x, y] = transform_coordinates([x, y], 'relative_display', 'absolute_pxl',full_size_image_shape)

    r = r/pxl_pitch
    return r, x, y

# 9
def cut_image(path, filename):
    """ Importing image from a given path. Applying Otsu filtering to
    obtain mask. Returns masked and cropped color image."""
    # Only MASTER_specks uses this for now (2019/10/30)! Have this in mind if using it elsewhere.

    # 2019/10/30 --> when analysing individual engines, ambient light may bleed through the darkness surrounding displays on acquired image. This causes irregularities in mask pattern and fails to properly cut the image. Added labeling to failproof display masking.

    # 2019/11 --> torough revision, algorithm adapted for dust detection
    # Detection chain goes as followa: FFT spatial filtering --> local otsu (r5) --> holes extraction --> histogram thresholding (1st point of separating potentially dirty and potentially clean engines) --> blobs dog
    # Clean engines most often have widespread slowly decaying histogram --> mean+4sigma > 25k? CLEAN:dirty (25k decided on empirically for now)
    # Second test is number of blobs. This is used to correct the initial assumption, if necessary:
    # If clean engine passes though first separation labeled as dirty, it is very likely it won't have any blob detected (as threshold for blob detection is much higher, specks are exclusively extreme outliers --> th_min = mean + 6*sigma.) --> this is the point where initial assumption can be corrected back to clean, if there are less than 10 blobs (number taken empirically for now)
    # It is also possible that dirty display is labeled as clean, but in test group of OEs this occured only for extremely dirty engines. This means that even it is not recognized in first step, additional check after blob detection fails the OE if it has more than 10 detected blobs.


    # Učitavanje slike (1)
    try:
        im = imageio.imread(path)
    except ValueError:
        print("Unable to read the image! File missing or truncated.")
        return None, 1

    print(filename)

    # Pretvorba slike (1) u greyscale
    img = rgb2gray(im)
    img = img_as_ubyte(img)
    img = exposure.rescale_intensity(img)

    # Postavke za Otsu Threshholding
    threshold_global_otsu = threshold_otsu(img)
    global_otsu = img >= threshold_global_otsu

    img[invert(global_otsu)] = [255]
    # plt.imshow(img, cmap=plt.cm.gray)
    # plt.show()

    # Label otsu mask and separate regions to find display
    labeled_mask = label(global_otsu, background=5)
    clusters = regionprops(labeled_mask, img)

    # Sort clusters after descending area size - first is background, second is display, rest is noise
    area_list = []
    for cluster in clusters:
        area_list.append(cluster.area)

    area_tuple = [(cluster.area, cluster.label) for cluster in clusters]
    sorted_area_tuple = sorted(area_tuple, reverse=True)
    display = clusters[sorted_area_tuple[1][1]-1]

    # Cropping the mask - alternative steps using properties of display cluster
    minr, minc, maxr, maxc = display.bbox
    minr, minc, maxr, maxc = minr-10, minc-10, maxr+10, maxc+10

    # FFT ANALIZA
    # FFT AND SPATIAL FILTERING
    from scipy import fftpack

    # Take the fourier transform of the image.
    # Then shift the quadrants around so that low spatial frequencies are in the center of the 2D fourier transformed image.
    F1 = fftpack.fft2(img)
    F2 = fftpack.fftshift(F1)

    # Calculate a 2D power spectrum
    psd2D = np.abs(F2)**2

    def create_circular_mask(h, w, center=None, radius=None):
        if center is None:  # use the middle of the image
            center = [int(w/2), int(h/2)]
        if radius is None:  # use the smallest distance between the center and image walls
            radius = min(center[0], center[1], w-center[0], h-center[1])

        Y, X = np.ogrid[:h, :w]
        dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

        mask = dist_from_center <= radius
        return mask

    # high pass
    hp = 0.03
    r_hp = int(img.shape[0]/2*hp)
    mask_disk_high_pass = create_circular_mask(img.shape[0], img.shape[1], radius=r_hp)

    # low pass filter
    lp = 0.20
    r_lp = int(img.shape[0]/2*lp)
    mask_disk_low_pass = create_circular_mask(img.shape[0], img.shape[1], radius=r_lp)

    # print("Mask radius is ", r_hp, r_lp)
    mask_disk_band_pass = mask_disk_low_pass*invert(mask_disk_high_pass)
    BAND_F2 = F2.copy()
    band_sigma = 20
    BAND_F2 = BAND_F2*filters.gaussian(mask_disk_band_pass, sigma=band_sigma)
    BAND_img = fftpack.ifft2(fftpack.ifftshift(BAND_F2)).real

    # high pass filter
    HP_F2 = F2.copy()
    HP_F2 = HP_F2*filters.gaussian(invert(mask_disk_high_pass))
    HP_img = fftpack.ifft2(fftpack.ifftshift(HP_F2)).real

    # low pass filter
    LP_F2 = F2.copy()
    LP_F2 = LP_F2*filters.gaussian(mask_disk_low_pass)
    LP_img = fftpack.ifft2(fftpack.ifftshift(LP_F2)).real

    # # PLOT FOR COMPARISON
    # fig, ax = plt.subplots(3, 3, figsize=(16, 16))
    # ax = ax.ravel()
    #
    # ax[0].imshow(img, cmap=plt.cm.gray)
    # ax[0].set_title('Original image')
    # ax[0].get_shared_x_axes().join(ax[0], ax[3], ax[6], ax[5], ax[8], ax[2])
    # ax[0].get_shared_y_axes().join(ax[0], ax[3], ax[6], ax[5], ax[8], ax[2])
    # # ax[0].axis('off')
    #
    # ax[1].imshow(np.log10(psd2D), cmap='viridis')
    # ax[1].set_title('Original image FFT')
    # ax[1].get_shared_x_axes().join(ax[1], ax[4], ax[7])
    # ax[1].get_shared_y_axes().join(ax[1], ax[4], ax[7])
    # # ax[1].axis('off')
    #
    # ax[2].imshow(BAND_img, cmap=plt.cm.gray)
    # ax[2].set_title('Band-pass img')
    # # ax[2].semilogy(psd1D)
    # # plt.xlabel('Spatial Frequency')
    # # plt.ylabel('Power Spectrum')
    # # # print(img.shape, "num = ", img.shape[0]/img.shape[1])
    # # # height = num*width
    # # ax[2].set_aspect(aspect=img.shape[0]/img.shape[1])
    # # ax[2].axvline(n, color='r')
    # # ax[2].axvline(ny, color='r')
    #
    # # plt.yscale('log')
    # # ax[2].axis('off')
    #
    # ax[3].imshow(HP_img, cmap=plt.cm.gray)
    # ax[3].set_title('High pass filtered image (low f eliminated)')
    #
    # # ax[4].imshow((20*np.log10(0.1 + F2)).astype(int), cmap='viridis')
    # ax[4].imshow(np.log10(np.abs(HP_F2)**2), cmap='viridis')
    # ax[4].set_title('High-pass filtered image FFT')
    # # ax[4].pl
    # # ax[1].axis('off')
    #
    # ax[5].imshow(img-HP_img, cmap=plt.cm.gray)
    # ax[5].set_title('original - HP_filter')
    #
    # ax[6].imshow(LP_img, cmap=plt.cm.gray)
    # ax[6].set_title('Low pass filtered image (high f eliminated)')
    #
    # # ax[4].imshow((20*np.log10(0.1 + F2)).astype(int), cmap='viridis')
    # ax[7].imshow(np.log10(np.abs(LP_F2)**2), cmap='viridis')
    # ax[7].set_title('Low-pass filtered image FFT')
    # # ax[1].axis('off')
    #
    # ax[8].imshow(img-LP_img, cmap=plt.cm.gray)
    # ax[8].set_title('original - LP_filter')
    #
    # plt.savefig('fft_overview_{}.png'.format(filename), bbox_inches='tight')
    # plt.show()
    # return None, None

    # # PLOT FOR COMPARISON - different layout
    # fig, ax = plt.subplots(2, 4, figsize=(22, 10))
    # ax = ax.ravel()
    #
    # ax[0].imshow(img, cmap=plt.cm.gray)
    # ax[0].set_title('Original image')
    # ax[0].get_shared_x_axes().join(ax[0], ax[1], ax[2], ax[3], ax[5], ax[6], ax[7])
    # ax[0].get_shared_y_axes().join(ax[0], ax[1], ax[2], ax[3], ax[5], ax[6], ax[7])
    #
    # ax[1].imshow(HP_img, cmap=plt.cm.gray)
    # ax[1].set_title('High pass filtered image (low f eliminated)')
    #
    # ax[2].imshow(LP_img, cmap=plt.cm.gray)
    # ax[2].set_title('Low pass filtered image (high f eliminated)')
    #
    # ax[3].imshow(BAND_img, cmap=plt.cm.gray)
    # ax[3].set_title('Band-pass img')
    #
    # ax[4].imshow(np.log10(np.abs(BAND_F2)**2), cmap='viridis')
    # # ax[4].imshow(np.log10(psd2D), cmap='viridis')
    # ax[4].set_title('Band-pass mask on image FFT')
    #
    # ax[5].imshow(img-HP_img, cmap=plt.cm.gray)
    # ax[5].set_title('original - HP_filter')
    #
    # ax[6].imshow(img-LP_img, cmap=plt.cm.gray)
    # ax[6].set_title('original - LP_filter')
    #
    # ax[7].imshow(img-BAND_img, cmap=plt.cm.gray)
    # ax[7].set_title('original - BAND_filter')
    #
    # plt.savefig('fft_overview2_{}-v{}.png'.format(filename, version), bbox_inches='tight')
    # plt.show()
    # plt.close()
    # return None, None

    filtered_img = BAND_img

    filtered_img = exposure.rescale_intensity(filtered_img)

    # LOCAL OTSU + mask
    radius = 5
    selem = disk(radius)
    local_otsu = filters.rank.otsu(filtered_img, selem, mask=global_otsu)

    local_otsu_rescale = exposure.rescale_intensity(local_otsu)

    # HOLES EXTRACTION
    seed = np.copy(local_otsu_rescale)
    seed[1:-1, 1:-1] = local_otsu_rescale.max()
    mask = local_otsu_rescale
    filled = morphology.reconstruction(seed, mask, method='erosion')

    # Plot intermediate holes extraction results
    seed = np.copy(local_otsu_rescale)
    seed[1:-1, 1:-1] = local_otsu_rescale.min()

    # rec = morphology.reconstruction(seed, mask, method='dilation')
    # fig, ax = plt.subplots(2, 2, figsize=(5, 4), sharex=True, sharey=True)
    # ax = ax.ravel()
    #
    # ax[0].imshow(local_otsu_rescale, cmap='gray')
    # ax[0].set_title('Original image')
    # ax[0].axis('off')
    #
    # ax[1].imshow(filled, cmap='gray')
    # ax[1].set_title('after filling holes')
    # ax[1].axis('off')
    #
    # ax[2].imshow(local_otsu_rescale-filled, cmap='gray')
    # ax[2].set_title('holes')
    # ax[2].axis('off')
    #
    # ax[3].imshow(local_otsu_rescale-rec, cmap='gray')
    # ax[3].set_title('peaks')
    # ax[3].axis('off')
    # plt.show()
    # return None, None

    holes = local_otsu_rescale-filled
    # plt.figure(figsize=(16, 14))
    # plt.imshow(holes, cmap=plt.cm.gray)
    # plt.title("holes = local_otsu_rescale - filled")
    # plt.savefig('holes-{}-v{}.png'.format(filename, version), bbox_inches='tight')
    # plt.show()
    # # plt.close()
    # return None, None

    mask_crop = invert(global_otsu)[minr:maxr, minc:maxc]  # create a cropped region of the gray ima
    mask_crop = morphology.binary_dilation(mask_crop, selem)
    # plt.figure(figsize=(16, 14))
    # plt.imshow(mask_crop, cmap=plt.cm.gray)
    # plt.title("mask_crop = binary dilation")
    # plt.savefig('mask_crop-{}-v{}.png'.format(filename, version), bbox_inches='tight')
    # plt.show()
    # plt.close()

    # Rescaling cropped holes to standard image uint range
    image_crop = abs(holes)[minr:maxr, minc:maxc]  # create a cropped region of the gray image
    image_crop[mask_crop] = [0]
    image_crop = exposure.rescale_intensity(image_crop, out_range=(0, 255)).copy()

    max = int(image_crop.max())
    for x in range(image_crop.shape[0]):
        for y in range(image_crop.shape[1]):
            image_crop[x, y] = image_crop[x, y]/max
    image_crop = img_as_uint(image_crop)

    filtered_image_crop = image_crop.copy()
    # identify outliers -- IQR
    # EXCLUDE ZERO-VALUED REGIONS FROM MEAN-STD ANALYSIS
    averaging_mask = filtered_image_crop == 0
    masked_filtered_image_crop = np.ma.array(filtered_image_crop, mask=averaging_mask)
    # plt.imshow(averaging_mask, cmap=plt.cm.viridis)
    # plt.show()
    # plt.imshow(masked_filtered_image_crop, cmap=plt.cm.viridis)
    # plt.show()

    im_mean = np.mean(masked_filtered_image_crop)
    im_std = np.std(masked_filtered_image_crop)
    # print("Masked mean and std (excludes zero-value regions): {}, {}".format(im_mean, im_std))

    # Provjera CISTO/PRLJAVO -->4*sigma dovoljno za procjenu
    flag_specks = False
    if(im_mean + 4*im_std >= 25000):
        print("th_min = {} >= 25k : Image most likely clean.".format(im_mean + 4*im_std))
    else:
        print("th_min = {} < 25k : Image most likely has specks.".format(im_mean + 4*im_std))
        flag_specks = True

    # Cutoffs for speck thresholding
    lower = im_mean - 6*im_std
    upper = im_mean + 6*im_std

    if(upper > image_crop.max()):
        # upper = im_mean
        # print("Min threshold changed from upper to mean value (upper > 65k)")
        print("Min threshold changed from upper to max value (upper > 65k)")
        # print("*********WARNING! WHEN DOES THIS OCCUR AND WHAT IS THE CONSEQUENCE OF THIS CHANGE?***********")
        upper = image_crop.max()

    # return None, None

    th_min = upper
    th_max = 50000

    # print("Lower threshold is {}".format(th_min))

    for x in range(filtered_image_crop.shape[0]):
        for y in range(filtered_image_crop.shape[1]):
            if (filtered_image_crop[x, y] < th_min or filtered_image_crop[x, y] > th_max):
                filtered_image_crop[x, y] = 0

    # histogram! --> u image_crop-temp izgleda kao da neke srednje vrijednosti daju ono sto trebam!!
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(21, 16))
    ax = axes.ravel()
    ax[0] = plt.subplot(2, 2, 1)
    ax[1] = plt.subplot(2, 2, 2)
    ax[2] = plt.subplot(2, 2, 3, sharex=ax[0], sharey=ax[0])
    ax[3] = plt.subplot(2, 2, 4, sharex=ax[1], sharey=ax[1])
    # ax[1].set_ylim([0, 1000])
    ax[1].set_ylim([0, 250000])

    ax[0].imshow(image_crop, cmap=plt.cm.viridis)
    ax[0].set_title('Original')
    ax[0].axis('off')
    ax[1].hist((image_crop).ravel(), bins=256)

    ax[1].set_title('Histogram')
    ax[1].axvline(th_min, color='r')
    ax[1].axvline(th_max, color='r')
    # ax[1].axvline(lower, color='g')
    ax[1].axvline(upper, color='g')
    ax[1].axvline(im_mean, color='g', linestyle=':')

    ax[2].imshow(filtered_image_crop, cmap=plt.cm.viridis)
    ax[2].set_title('Thresholded')
    ax[2].axis('off')

    ax[3].hist(filtered_image_crop.ravel(), bins=256)
    ax[3].set_title('Histogram - filtered')
    ax[3].axvline(th_min, color='r')
    ax[3].axvline(th_max, color='r')

    plt.savefig('specks_histogram-{}.png'.format(filename), bbox_inches='tight')
    plt.close()

    # BLOB detection
    im_crop = im[minr:maxr, minc:maxc]  # create a cropped region of the gray ima
    test_img = filtered_image_crop

    blobs_dog = blob_dog(test_img, threshold=.01, min_sigma=10, sigma_ratio=1.1)
    blobs_dog[:, 2] = blobs_dog[:, 2] * sqrt(2)

    blobs_list = [blobs_dog]
    color_blob = ['lime']
    titles = ['Difference of Gaussian']
    sequence = zip(blobs_list, color_blob, titles)

    # Raise flag for dirty images, even if they passed earlier assesment
    print("Number of detected blobs = {}".format(len(blobs_dog)))
    print("\tflag_specks before blob check: {}".format(flag_specks))

    # Raise or correct flags
    # If display was labeled clean, but has more than 10 detected blobs --> FAIL
    if(not flag_specks and len(blobs_dog) > 10):
        flag_specks = True

    # If display was labeled dirty, but has no detected blobs --> PASS
    if(len(blobs_dog) == 0):
        flag_specks = False

    print("\tflag_specks after blob check: {}".format(flag_specks))
    if(flag_specks):
        print("FAIL!\n")
    else:
        print("PASS!\n")


    fig, axes = plt.subplots(1, 2, figsize=(21, 16), sharex=True, sharey=True)
    ax = axes.ravel()
    for idx, (blobs, color, title) in enumerate(sequence):
        ax[idx].set_title(title)
        ax[idx].imshow(im_crop)
        for blob in blobs:
            y, x, r = blob
            c = plt.Circle((x, y), r*1.5, color=color, linewidth=2, fill=False)
            ax[idx].add_patch(c)
        ax[idx].set_axis_off()
    ax[1].imshow(test_img)
    plt.tight_layout()

    plt.savefig('specks-{}.png'.format(filename), bbox_inches='tight')
    plt.close()

    return flag_specks, 0


# 10
def intensity_check(masked_image, mask, filename, path, report_path):
    from skimage.color import rgb2hsv, rgb2yiq
    rgb_img = masked_image

    h = masked_image.shape[0]
    w = masked_image.shape[1]
    # print(h, w)

    # HSV
    hsv_img = rgb2hsv(rgb_img)
    h_img = hsv_img[:, :, 0]
    s_img = hsv_img[:, :, 1]
    v_img = hsv_img[:, :, 2]

    H = np.mean(np.ma.array(h_img, mask=mask))
    S = np.mean(np.ma.array(s_img, mask=mask))
    V = np.mean(np.ma.array(v_img, mask=mask))
    # print(H, S, V)

    # fig, (ax0, ax1, ax2) = plt.subplots(ncols=3, figsize=(16, 9), sharex=True, sharey=True)
    #
    # ax0.imshow(h_img, cmap='viridis')
    # ax0.set_title("Hue channel")
    # ax0.axis('off')
    # ax1.imshow(s_img, cmap='viridis')
    # ax1.set_title("Saturation channel")
    # ax1.axis('off')
    # ax2.imshow(v_img, cmap='viridis')
    # ax2.set_title("Value channel")
    # ax2.axis('off')
    #
    # fig.tight_layout()
    # # plt.savefig('HSV-{}.png'.format(filename))
    # plt.show()
    # # plt.close()


    # YIQ
    yiq_img = rgb2yiq(rgb_img)
    y_img = yiq_img[:, :, 0]
    i_img = yiq_img[:, :, 1]
    q_img = yiq_img[:, :, 2]

    Y = np.mean(np.ma.array(y_img, mask=mask))
    I = np.mean(np.ma.array(i_img, mask=mask))
    Q = np.mean(np.ma.array(q_img, mask=mask))

    # print(Y, I, Q)

    # fig, (ax0, ax1, ax2) = plt.subplots(ncols=3, figsize=(16, 9), sharex=True, sharey=True)
    #
    # ax0.imshow(y_img)
    # ax0.set_title("Y channel")
    # ax0.axis('off')
    # ax1.imshow(i_img)
    # ax1.set_title("I channel")
    # ax1.axis('off')
    # ax2.imshow(q_img)
    # ax2.set_title("Q channel")
    # ax2.axis('off')
    #
    # fig.tight_layout()
    # plt.savefig('YIQ-{}.png'.format(filename))
    # # plt.show()
    # plt.close()

    # RGB
    # r_img = rgb_img[:, :, 0]
    # g_img = rgb_img[:, :, 1]
    # b_img = rgb_img[:, :, 2]
    #
    # R = np.mean(np.ma.array(r_img, mask=mask))
    # G = np.mean(np.ma.array(g_img, mask=mask))
    # B = np.mean(np.ma.array(b_img, mask=mask))
    # print(R, G, B)
    #
    # R = r_img.mean()
    # G = g_img.mean()
    # R = r_img.mean()
    # print(R, G, B)

    oe_name = filename[filename.find("OE"):]
    print(oe_name)

    if ("white" in filename):
        h_mean = 0.66
        h_std = 0.01

        s_mean = 0.13
        s_std = 0.02

        v_mean = 0.99939
        v_std = 0.00009

        y_mean = 0.90
        y_std = 0.02

        i_mean = -0.038
        i_std = 0.007

        q_mean = 0.035
        q_std = 0.008

    elif ("red" in filename):
        h_mean = 0.88
        h_std = 0.03

        s_mean = 0.937
        s_std = 0.002

        v_mean = 0.85
        v_std = 0.02

        y_mean = 0.295
        y_std = 0.008

        i_mean = 0.47
        i_std = 0.01

        q_mean = 0.177
        q_std = 0.005

    elif ("green" in filename):
        h_mean = 0.358
        h_std = 0.002

        s_mean = 0.793
        s_std = 0.002

        v_mean = 0.89
        v_std = 0.03

        y_mean = 0.61
        y_std = 0.02

        i_mean = -0.227
        i_std = 0.009

        q_mean = -0.335
        q_std = 0.009

    elif ("blue" in filename):
        h_mean = 0.655
        h_std = 0.004

        s_mean = 0.897
        s_std = 0.002

        v_mean = 0.9995
        v_std = 0.0001

        y_mean = 0.24
        y_std = 0.01

        i_mean = -0.306
        i_std = 0.005

        q_mean = 0.25
        q_std = 0.01

    HSV_bool = ["", "", ""]
    YIQ_bool = ["", "", ""]

    # HSV
    if (H <= h_mean+4*h_std) and (H >= h_mean-4*h_std):
        HSV_bool[0] = "PASS"
    else:
        HSV_bool[0] = "FAIL"

    if (S <= s_mean+4*s_std) and (S >= s_mean-4*s_std):
        HSV_bool[1] = "PASS"
    else:
        HSV_bool[1] = "FAIL"

    if (V <= v_mean+4*v_std) and (V >= v_mean-4*v_std):
        HSV_bool[2] = "PASS"
    else:
        HSV_bool[2] = "FAIL"

    # YIQ
    if (Y <= y_mean+4*y_std) and (Y >= y_mean-4*y_std):
        YIQ_bool[0] = "PASS"
    else:
        YIQ_bool[0] = "FAIL"

    if (I <= i_mean+4*i_std) and (I >= i_mean-4*i_std):
        YIQ_bool[1] = "PASS"
    else:
        YIQ_bool[1] = "FAIL"

    if (Q <= q_mean+4*q_std) and (Q >= q_mean-4*q_std):
        YIQ_bool[2] = "PASS"
    else:
        YIQ_bool[2] = "FAIL"


    print ("HSV: {}".format(HSV_bool))
    print("YIQ: {}".format(YIQ_bool))

    if (HSV_bool.count("PASS") == 3) and (YIQ_bool.count("PASS") == 3):
        flag = False
    else:
        flag = True


    # Create report for qtiplot
    # if ("white" in filename):
    #     report_path += '\\REPORT - INTENSITY - white.txt'
    # elif ("red" in filename):
    #     report_path += '\\REPORT - INTENSITY - red.txt'
    # elif ("green" in filename):
    #     report_path += '\\REPORT - INTENSITY - green.txt'
    # elif ("blue" in filename):
    #     report_path += '\\REPORT - INTENSITY - blue.txt'


    t = datetime.today()
    date = "{}.{}.{}".format(t.day, t.month, t.year)
    time = "{:02}:{:02}:{:02}".format(t.hour, t.minute, t.second)

    report_path += '\\REPORT - INTENSITY.txt'
    text_file = open(report_path, 'a+')
    text_file.write("REPORT - INTENSITY\n")
    text_file.write("{} @ {}:\n".format(date, time))
    text_file.write("{}\n".format(filename))
    text_file.write("HSV: {}\nYIQ: {}\n".format(HSV_bool, YIQ_bool))
    # text_file.write("{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\t{}\n".format(R, G, B, H, S, V, Y, I, Q, oe_name))

    if(flag):
        text_file.write("FAIL\n\n")
    else:
        text_file.write("PASS\n\n")

    text_file.close()

    return flag


# # *11
# def flare_check(path, filename):
#     # 2019/12/02 Adapted from WIP cut_images
#     # Učitavanje slike (1)
#     try:
#         im = imageio.imread(path)
#     except ValueError:
#         print("Unable to read the image! File missing or truncated.")
#         return None, 1
#
#     # Pretvorba slike (1) u greyscale
#     img = rgb2gray(im)
#     img = img_as_ubyte(img)
#     img = exposure.rescale_intensity(img)
#
#     # Postavke za Otsu Threshholding
#     threshold_global_otsu = threshold_otsu(img)
#     global_otsu = img >= threshold_global_otsu
#     # img[invert(global_otsu)] = [255]
#     # plt.imshow(img, cmap=plt.cm.gray)
#     # plt.show()
#     # return
#
#     # # Analiza flare korone
#     flare_otsu = img
#     flare_otsu[global_otsu] = [0]
#     # img[invert(global_otsu)] = [255]
#     plt.figure(figsize=(16, 10))
#     plt.imshow(flare_otsu, cmap=plt.cm.gray)
#     plt.savefig('binary-{}_before.png'.format(filename), bbox_inches='tight')
#     # plt.show()
#     plt.close()
#
#     # print(flare_otsu.mean())
#     print("Flare otsu (mean, min, max) = ({}, {}, {}) ".format(flare_otsu.mean(), flare_otsu.min(), flare_otsu.max()))
#
#     text_file = open('qti.txt', 'a+')
#     text_file.write("{}\t\t".format(filename))
#     text_file.write("{}\n".format(flare_otsu.mean()))
#     text_file.close()
#
#     return None, None


# 12. 

def cut_slice(path):
    try:
        im = imageio.imread(path)
    except ValueError:
        print("Unable to read the image! File missing or truncated.")
        return None, None, 1

    # Pretvorba slike (1) u greyscale
    im_g = rgb2gray(im)
    img = img_as_ubyte(im_g)

    # Uvođenje blura da se šum nakon otsu tehnike ne uvede u masku
    img = ndimage.gaussian_filter(img, 1)
    
    mean = int((img.shape[0])/2)
    slice_cord = img[mean, :]
    plt.plot(slice_cord)
    plt.savefig('slice_with_backgroud.png', bbox_inches='tight')
    plt.show()
    
    display = [x for x in slice_cord if x > 6]
    display = np.array(display)
    plt.plot(display)
    np.set_printoptions(threshold=np.inf)
    print(display)
    print(np.mean(display))
    print(np.median(display))
    if (np.mean(display) > np.median(display)):
        print("TRUE")
    else:
        print("FALSE")
    plt.savefig('slice_signal.png', bbox_inches='tight')
    plt.show()
    
    return display
    
    