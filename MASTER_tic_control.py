import tic_control as tc
import time
import subprocess
import yaml
import sys

# print("HEllo world")
# print(sys.argv)
keyword = sys.argv[1]

#  Available keywords: HOME, D, S12, S3, S45, S6, CALIBRATE
# Absolute coordinates go from -500 to 500

if(keyword == "CALIBRATE"):
    tc.calibrate()
elif(keyword == "HOME"):
    tc.go_home()
elif(keyword == "D"):
    tc.change_focus(350)
elif(keyword == "S123"):
    tc.change_focus(-150)
elif(keyword == "S4"):
    tc.change_focus(-400)
elif(keyword == "S5"):
    tc.change_focus(-480)
elif(keyword == "S6"):
    tc.change_focus(-80)
else:
    print("ERROR!")
