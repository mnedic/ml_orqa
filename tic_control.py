# Uses ticcmd to send and receive data from the Tic over USB.
# Works with either Python 2 or Python 3.
#
# NOTE: The Tic's control mode must be "Serial / I2C / USB".
import subprocess
import yaml
import time

def ticcmd(*args):
    return subprocess.check_output(['ticcmd'] + list(args))


def set_position(value, reference):
    # # reference is "absolute" or "relative", and value is corresponding change of position
    print("Setting target position to {} {}.".format(reference, value))
    if (reference == "absolute"):
        ticcmd('--exit-safe-start', '--position', str(value))
    else:
        ticcmd('--exit-safe-start', '--position-relative', str(value))


def check_position():
    status = yaml.load(ticcmd('-s', '--full'), Loader=yaml.FullLoader)
    position = status['Current position']
    # print("Current position is {}.".format(position))
    # print(status)
    return position

def calibrate():
    # Setting parameters
    # Go home
    home = 0
    ticcmd('--exit-safe-start', '--position', str(home))
    # Set speed limit to 150.0000 pulses/s (1500000)
    max_speed_limit = 1500000
    ticcmd('--exit-safe-start', '--max-speed', str(max_speed_limit))
    # Set acc and decc limits to 50.00 pulses/s**2 (5000)
    acc_limit = 5000
    dec_limit = acc_limit
    ticcmd('--exit-safe-start', '--max-accel', str(acc_limit))
    ticcmd('--exit-safe-start', '--max-decel', str(dec_limit))
    # Set step mode to 1/2
    step = "half"
    ticcmd('--exit-safe-start', '--step-mode', step)
    # Set current limit to 174mA
    current_limit = 174
    ticcmd('--exit-safe-start', '--current', str(current_limit))


def go_home():
    # GO HOME
    home = 500
    init = check_position()
    if (home != init):
        set_position(home, "absolute")
        while (check_position() != home):
            time.sleep(0.01)
    else:
        print("Already home!")

    print("\tGO HOME finished. Current position: {}".format(check_position()))

def change_focus(target):
    # init = check_position()
    # target = -500 if init > 0 else 500
    set_position(target, "absolute")
    while (check_position() != target):
        time.sleep(0.01)
    print("Target reached!")
