
# Modules import

import image_analysis_module2
from skimage.color import rgb2gray
from os import listdir
from os.path import isfile, join
import os
import sys
import time

#print("\n****************************** Slice detection ******************************")
#print("******************************    Start of code.    ****************************")
# Change directory to ACQUISITION folder. Find the OE directory that needs to be analyzed
dir_path = os.path.dirname(os.path.realpath(__file__))
acquisition_path = dir_path + "\\ACQUISITION"
os.chdir(acquisition_path)

OE_name = str(sys.argv[1])
OE_dir = [f for f in listdir(acquisition_path) if f == OE_name]

if(len(OE_dir) != 1):
    # print(OE_name, OE_dir)
    print("{} not found!".format(OE_name))
    quit()

# Iterate current test for all eligible images found in current OE
temp_dir = acquisition_path + "\\" + OE_name + "\\ScreenImages"
onlyfiles = [f for f in listdir(temp_dir) if isfile(join(temp_dir, f))]

# Create new folder for results
myfolder = "SliceResult"
image_analysis_module2.mkdir_analysis(acquisition_path + "\\" + OE_name + '\\' + myfolder)
os.chdir(acquisition_path + "\\" + OE_name + '\\' + myfolder)

# Import images
paths = image_analysis_module2.import_display_images(temp_dir)
filename = os.path.splitext(os.path.basename(paths[0]))[0]


# Load reference image - white screen
mask, mask_crop, readErr = image_analysis_module2.otsu_cut_and_mask(paths[0])

# Loop analysis over all images that meet the requirement (checkers)
i = 0
for path in paths:
    if not ("lpmm" in path) or ("white" in path):
        continue

    #print('\n{} in {} START....................................................'.format(i+1, OE_name))
    # Grayscale image and cut the centar part of image
    filename = os.path.splitext(os.path.basename(path))[0]
    #print(filename)

    image_crop, masked_image, masked_image_gray, readErr = image_analysis_module2.apply_mask(path, mask, mask_crop)

    warning = image_analysis_module2.magic_flip_test(masked_image_gray, mask, filename, path, acquisition_path + "\\" + OE_name + '\\' + myfolder)



i += 1


os.chdir(dir_path)
#print("\n****************************** Slice Detection ******************************")
#print("******************************    End of code.    ****************************")
