import numpy as np
import tensorflow.keras 
from PIL import Image
import argparse
import parser
import warnings
warnings.filterwarnings("ignore")
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
from tensorflow.keras.models import load_model
import pathlib

batch_size = 32
img_height = 80
img_width = 120


def ImagePreProcessing(image_path):
    img = Image.open(image_path)
    img = img.resize((img_width, img_height))
    final_image = np.array(img)
    final_image = final_image[:, :, :3]
    print(final_image.shape)
    return final_image



def pred_to_decision(pred):
    if pred>=0.5:
        M_F = 'FLIPPED'
    else:
        M_F = 'NORMAL'
    return M_F

def decision_to_prob(prediction,decision):
    if decision=='NORMAL':
        p = (1-prediction) * 100
    else:
        p = prediction * 100
    return p

def main():
    
    args = parser.parse_args()
    im_path = args.image_path
    # stara optika
    #model_path = "C:\\Users\\Korisnik\\Desktop\\DATA\\Acquisition-server\\classifier.h5"  
    # nova optika    
    model_path = "C:\\Users\\Korisnik\\Desktop\\DATA\\Acquisition-server\\classifier_new.h5"
    image = ImagePreProcessing(im_path)
    model = load_model(model_path)
    prediction = model.predict(np.array([image]))[0][0]
    #print(prediction)
    decision = pred_to_decision(prediction)
    f = decision_to_prob(prediction=prediction, decision=decision)
    print('The prediction for the input image is: ' + str(decision) + ' with probability %.2f' %f + '%')
    return decision
if __name__=='__main__':
    
    parser = argparse.ArgumentParser(formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--image_path',type=str,help = 'Path of the png image'),
    #parser.add_argument('--model_path',type=str,help = 'Path of the model'),
    
    main()