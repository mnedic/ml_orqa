import image_analysis_module
from shutil import copy, rmtree
import os
from os import listdir
from os.path import isfile, join, exists
import sys
import shutil


temp = str(sys.argv[1])
temp = "ACQUISITION" + "\\" + temp


# If OE already exists, remove the old map first
if(exists(temp)):
	shutil.rmtree(temp, ignore_errors=True)
    #rmtree(temp)

# Create map named after data from QR code
image_analysis_module.mkdir_analysis(temp)

# Copy other Acquisition Files to created folder
dir_path = os.path.dirname(os.path.realpath(__file__))
origin = dir_path
onlyfiles = [f for f in listdir(dir_path) if isfile(join(dir_path, f))]
# print(onlyfiles)
for filepath in onlyfiles:
    if filepath.endswith(".exe") or ("MASTER_projectScreen.py" in filepath):
        copy(filepath, dir_path + "\\" + temp)
        # print(filepath)
