import os
from os import listdir
from os.path import isfile, join, exists
import sys
from datetime import datetime

# Readout of TestImages  --> auto detection of number and name of test images - no need to manually change renaming procedure if additional test images are necesarry
VARIABLE_FOCUS = False
colors = []
if VARIABLE_FOCUS:
    surfaces = ["S123", "S4", "S5", "S6"]
else:
    surfaces = []

test_path = os.path.dirname(os.path.realpath(__file__))
test_dir = test_path + "\\TestImages"
onlyfiles = [f for f in listdir(test_dir) if isfile(join(test_dir, f))]

for file in onlyfiles:
    if file.endswith(".png"):
        start = '_'
        end = '-pure'
        colors.append(file[file.find(start)+len(start):file.find(end)])

# print(colors, len(colors))
# quit()

# Readout of acquired images
full_path = str(sys.argv[1])
temp_dir = full_path + "\\ScreenImages"
onlyfiles = [f for f in listdir(temp_dir) if isfile(join(temp_dir, f))]

# print(onlyfiles)
# quit()

# check if number of test images matches number of acquired images --> write a warning if it does not
N_tests = len(colors) + len(surfaces)
if N_tests != len(onlyfiles):
    warning_path = test_path + '\\ACQUISITION\\WARNING.txt'
    if(exists(warning_path)):
        text_file = open(warning_path, 'a')
        text_file.write("\n")
    else:
        text_file = open(warning_path, 'w')
#
    text_file.write("WARNING - ({})\nRENAMING UNREALIABLE!\nNumber of TestImages differs from number of acquired images.\n\tCheck ScreenImages, remove the excess or repeat the measurements.\n".format(full_path[full_path.find("ACQUISITION\\")+len("ACQUISITION\\"):]))
    print("WARNING - ({})\nRENAMING UNREALIABLE!\nNumber of TestImages differs from number of acquired images.\n\tCheck ScreenImages, remove the excess or repeat the measurements.\n")
    t = datetime.today()
    text_file.write("--- timestamp: {}.{}.{} @ {:02}:{:02}:{:02} ---\n".format(t.day, t.month, t.year, t.hour, t.minute, t.second))

    text_file.close()
    print("ERROR! Check warning log.")


# Rename all acquired images
i = 0
names = colors.copy()
# print(names)
names.extend(surfaces)
# print(names)
# quit()

for file, name in zip(onlyfiles, names):
    start = 'ACQUISITION\\'
    dir = full_path[full_path.find(start)+len(start):]
    old = temp_dir + "\\" + file
    new = temp_dir + "\\" + str(i) + "_" + name + "_" + dir + ".png"
    os.rename(old, new)
    i += 1
