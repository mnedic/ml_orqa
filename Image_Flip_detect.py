import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential
import pandas as pd 
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from PIL import Image
import matplotlib.image as mpg


import pathlib
## stara optika fpv.1
#data_dir = pathlib.Path("C:\\Users\\Korisnik\\Desktop\\train_downPeak")
## nova optika fpv.2
data_dir = pathlib.Path("C:\\Users\\Korisnik\\Desktop\\Orqa_fpv2_test")

batch_size = 32
img_height = 80
img_width = 120

train_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  image_size=(img_height, img_width),
  batch_size=batch_size)


class_names = train_ds.class_names
print(class_names)
img = []
label = []
for images, labels in train_ds.take(1):
  for i in range(12):
    img.append(images[i])
    if class_names[labels[i]] == "FLIP":
        label.append(1)
    else:
        label.append(0)

images = np.array(img)
labels = np.array(label)
print(labels)


classifier = Sequential()
img_height = 80
img_width = 120
# Step 1 - Convolution
classifier.add(Conv2D(64, (2, 2), input_shape = (img_height,img_width, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))

classifier.add(Conv2D(64, (2, 2), input_shape = (img_height,img_width, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))

classifier.add(Flatten())

#classifier.add(Dense(units = 32, activation = 'relu'))
classifier.add(Dense(units = 1, activation = 'sigmoid'))

# Compiling the CNN
classifier.summary()

train_images, test_images, train_labels, test_labels = train_test_split(images, labels, test_size=0.2)

classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
history = classifier.fit(train_images, train_labels, epochs=15, validation_data=(test_images, test_labels),batch_size=10)

loss = history.history['accuracy']
val_loss = history.history['val_accuracy']

plt.plot(np.arange(1,len(loss)+1,1),loss,color='navy', label = 'Accuracy')
plt.plot(np.arange(1,len(loss)+1,1),val_loss,color='red',label='Validation Accuracy')
plt.legend(fontsize=15)
plt.show()

classifier.save("classifier_new.h5")
print("Saved model to disk")