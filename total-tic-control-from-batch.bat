@echo off

   set STARTTIME=%TIME%
   @echo off

   start "MAIN" /WIN cmd
   setlocal disableDelayedExpansion

   set myvar=%1
   cd /d D:\Acquisition-server

   START "" /b /wait python preparation.py %myvar%
   REM goto:eof
   REM PAUSE

   set mypath=%cd%
   set fullpath=%mypath%\ACQUISITION\%myvar%
   REM echo %fullpath%
   REM goto:eof

   START "" /b /wait python MASTER_tic_control.py CALIBRATE

   echo Set camera (0cm), focus (nearest near) and lenslets (NONE) to HOME position.
   START "" /b /wait python MASTER_tic_control.py HOME

   echo Set focus (display) to D position.
   START "" /b /wait python MASTER_tic_control.py D

   for /r %%F in (*pure*.png) do (
     REM echo "%%F"
     cd %fullpath%
     REM goto:eof
     START "CINEMA" /b Project_opencv.exe "%%F"

     echo "Executing image acquisition."
     START "L" /b R_BasicFunction.exe
     timeout /t 6 /nobreak
     taskkill /F /FI "IMAGENAME eq R_BasicFunction.exe"
     taskkill /F /FI "WINDOWTITLE eq ProjectScreen"
     cd..
     cd..
  )


  set f=%mypath%\TestImages\01_white-pure_4-3.png

  echo Set camera (3cm), focus (surfaces 1, 2, 3) and lenslets (A) to S123 position.
  START "" /b /wait python MASTER_tic_control.py S123

  cd %fullpath%
  START "CINEMA" /b Project_opencv.exe "%f%"
  echo "Executing image acquisition."
  START "L" /b R_BasicFunction.exe
  timeout /t 6 /nobreak
  taskkill /F /FI "IMAGENAME eq R_BasicFunction.exe"
  taskkill /F /FI "WINDOWTITLE eq ProjectScreen"
  cd..
  cd..


 echo Set focus to S4 position. (surfaces 4)
 START "" /b /wait python MASTER_tic_control.py S4

 cd %fullpath%
 START "CINEMA" /b Project_opencv.exe "%f%"
 echo "Executing image acquisition."
 START "L" /b R_BasicFunction.exe
 timeout /t 6 /nobreak
 taskkill /F /FI "IMAGENAME eq R_BasicFunction.exe"
 taskkill /F /FI "WINDOWTITLE eq ProjectScreen"
 cd..
 cd..


echo Set focus to S5 position. (surfaces 5)
START "" /b /wait python MASTER_tic_control.py S5

cd %fullpath%
START "CINEMA" /b Project_opencv.exe "%f%"
echo "Executing image acquisition."
START "L" /b R_BasicFunction.exe
timeout /t 6 /nobreak
taskkill /F /FI "IMAGENAME eq R_BasicFunction.exe"
taskkill /F /FI "WINDOWTITLE eq ProjectScreen"
cd..
cd..


echo Set focus and lenslets (B) to S6 position. (surfaces 6)
START "" /b /wait python MASTER_tic_control.py S6

cd %fullpath%
START "CINEMA" /b Project_opencv.exe "%f%"
echo "Executing image acquisition."
START "L" /b R_BasicFunction.exe
timeout /t 6 /nobreak
taskkill /F /FI "IMAGENAME eq R_BasicFunction.exe"
taskkill /F /FI "WINDOWTITLE eq ProjectScreen"
cd..
cd..


echo Set cam back to 0cm, remove lenslets, focus to HOME. (500)
START "" /b /wait python MASTER_tic_control.py HOME

set ENDTIME=%TIME%
echo %ENDTIME%

rem ******************  END of MAIN CODE SECTION ******************


rem Change formatting for the start and end times
for /F "tokens=1-4 delims=:.," %%a in ("%STARTTIME%") do (
  set /A "start=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
)

for /F "tokens=1-4 delims=:.," %%a in ("%ENDTIME%") do (
  set /A "end=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
)

rem Calculate the elapsed time by subtracting values
set /A elapsed=end-start

rem Format the results for output
set /A hh=elapsed/(60*60*100), rest=elapsed%%(60*60*100), mm=rest/(60*100), rest%%=60*100, ss=rest/100, cc=rest%%100
if %hh% lss 10 set hh=0%hh%
if %mm% lss 10 set mm=0%mm%
if %ss% lss 10 set ss=0%ss%
if %cc% lss 10 set cc=0%cc%

set DURATION=%hh%:%mm%:%ss%,%cc%

echo Acquisition of %myvar% Done (%DATE% %TIME%) >> timer.txt
echo Start    : %STARTTIME% >> timer.txt
echo Finish   : %ENDTIME% >> timer.txt
echo          --------------- >> timer.txt
echo Duration : %DURATION% >> timer.txt
echo. >> timer.txt

REM PAUSE
REM cd..
START "" /b /wait python temp_clean.py
START "" /b /wait python rename.py %fullpath%
REM PAUSE
REM EXIT
