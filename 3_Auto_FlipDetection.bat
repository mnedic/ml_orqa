@echo off

   rem ******************  MAIN CODE SECTION
   set STARTTIME=%TIME%
   echo %TIME%
   @echo off

   REM start "MAIN" /WIN /C cmd
   setlocal disableDelayedExpansion

   set myvar=%1
   rem echo %myvar%
   
   set mypath=%cd%
   echo %mypath%
   set imgSrc=%mypath%\ACQUISITION\%myvar%\SliceResult\image_slice_lpmm_1_12lpmm_%myvar%.png
   REM set imgSrc=%mypath%\ACQUISITION\%myvar%\ResultSharpness\image_slice_lpmm.png
   REM echo %imgSrc%
   
   START "Slice Detection" /b /WAIT python slice_detection.py %myvar%
   echo Making image - Done (%TIME%)
   REM echo %imgSrc% >> images.txt

   Start "Prediction" /b /WAIT python classifier.py --image %imgSrc%
   REM START /b /WAIT C:\Users\Korisnik\source\repos\ML_dowPeak_Signal\ML_dowPeak_Signal\bin\Debug\netcoreapp3.1\ML_dowPeak_Signal.exe %imgSrc%
   REM START /b /WAIT C:\Users\Korisnik\source\repos\ML_pureSignal\ML_pureSignal\bin\Debug\netcoreapp3.1\ML_pureSignal.exe %imgSrc%
   REM START /b /WAIT C:\Users\Korisnik\source\repos\ML_downPeak_low\ML_downPeak_low\bin\Debug\netcoreapp3.1\ML_downPeak_low.exe %imgSrc%
   REM START /b /WAIT C:\Users\Korisnik\Desktop\AnaML\AnaMLML.ConsoleApp\bin\Release\netcoreapp3.1\mltool.exe  %imgSrc%
   set ENDTIME=%TIME%

   rem ******************  END MAIN CODE SECTION



rem Change formatting for the start and end times
for /F "tokens=1-4 delims=:.," %%a in ("%STARTTIME%") do (
  set /A "start=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
)

for /F "tokens=1-4 delims=:.," %%a in ("%ENDTIME%") do (
  set /A "end=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
)

rem Calculate the elapsed time by subtracting values
set /A elapsed=end-start

rem Format the results for output
set /A hh=elapsed/(60*60*100), rest=elapsed%%(60*60*100), mm=rest/(60*100), rest%%=60*100, ss=rest/100, cc=rest%%100
if %hh% lss 10 set hh=0%hh%
if %mm% lss 10 set mm=0%mm%
if %ss% lss 10 set ss=0%ss%
if %cc% lss 10 set cc=0%cc%

set DURATION=%hh%:%mm%:%ss%,%cc%

echo Test FLIP Orientation of %myvar% Done (%DATE% %TIME%) >> timer.txt
echo Start    : %STARTTIME% >> timer.txt
echo Finish   : %ENDTIME% >> timer.txt
echo          --------------- >> timer.txt
echo Duration : %DURATION% >> timer.txt
echo. >> timer.txt