import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import pathlib
import time 

start = time.time()
batch_size = 32
img_height = 180
img_width = 240
class_names = ['FLIP', 'NORM']

# load and evaluate a saved model
from tensorflow.keras.models import load_model

# load model
model = load_model('model.h5')

test_image = pathlib.Path("C:\\Users\\Korisnik\\Desktop\\DATA\\Acquisition-server\\ACQUISITION\\C3_flipped_low\\SliceResult\\image_slice_lpmm.png")

img = keras.preprocessing.image.load_img(
    test_image, target_size=(img_height, img_width)
)

img_array = keras.preprocessing.image.img_to_array(img)
img_array = tf.expand_dims(img_array, 0)

predictions = model.predict(img_array)
score = tf.nn.softmax(predictions[0])

print("This image most likely belongs to {} with a {:.2f} percent confidence.".format(class_names[np.argmax(score)], 100 * np.max(score)))

end = time.time()
duration = end - start
print("Time : ", duration) # CPU seconds elapsed (floating point)